# COFYflex Python

## Description
Python library containing the source code for multiple microservices used by COFYcloud to provide flexibility services.

## Visuals
TODO: Add viz of Price Matrix
See Demo Notebook

## Installation
`python3 -m pip install cofyflex`

## Usage
See Demo Notebooks

## Support
Use the Gitlab issue tracker, or see docs.cofybox.io.

## Roadmap
TODO

## Contributing

- Clone the repo
- Create a virtual environment (`python3 -m venv venv`) & Activate it (`source venv/bin/activate`)
- Install requirements (`pip install -r requirements.txt`)
- Install additional packages for running notebooks and visualisation (`pip install jupyter matplotlib`)
- Start notebook server and run the examples (`jupyter notebook`)

Please contribute by creating a branch per issue and using merge requests that are easily testable.

## Authors and acknowledgment
The DEV-team of the H2020 REScoopVPP project: EnergyID & CarbonCoop

## License
MIT